from experta import *
from PyQt5 import *
import sys  # sys нужен для передачи argv в QApplication
from PyQt5 import QtWidgets
from design import Ui_MainWindow
from Core import Core

#engine = Core()
#engine.reset()
#engine.declare(Fact(action="FORWARD"), Fact(best_time='skip'), Fact(average_time='skip'), Fact(worst_time='skip'), Fact(memory='skip'), Fact(sustainability='yes'), Fact(exchanges='nlogn'))
#engine.declare(Fact(action="BACKWARD"), Fact(timsort_may_be_used=True), Fact(merge_sort_may_be_used=True))

#engine.run()  # Run it!
#engine.declare(Fact(final=True))
#engine.run()  # Run it!
#print(engine.facts)


class App(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        # Это здесь нужно для доступа к переменным, методам
        # и т.д. в файле design.py
        super().__init__()
        self.setupUi(self)  # Это нужно для инициализации нашего дизайна
        self.engine = Core()
        self.pushButton.clicked.connect(self.reason)
        self.pushButton_2.clicked.connect(self.reason_backward)

    def reason_backward(self):
        print("----------------------------------------------------------")
        self.engine.reset()  # Prepare the engine for the execution.
        if self.bubbleSortCb.isChecked():
            self.engine.declare(Fact(bubble_sort_may_be_used=True))
        if self.insertionSortCb.isChecked():
            self.engine.declare(Fact(insertion_sort_may_be_used=True))
        if self.shellsortCb.isChecked():
            self.engine.declare(Fact(shellsort_may_be_used=True))
        if self.selectionSortCb.isChecked():
            self.engine.declare(Fact(selection_sort_may_be_used=True))
        if self.quickSortCb.isChecked():
            self.engine.declare(Fact(quick_sort_may_be_used=True))
        if self.mergeSortCb.isChecked():
            self.engine.declare(Fact(merge_sort_may_be_used=True))
        if self.timsortCb.isChecked():
            self.engine.declare(Fact(timsort_may_be_used=True))
        if self.heapSortCb.isChecked():
            self.engine.declare(Fact(heap_sort_be_used=True))
        if self.smoothsortCb.isChecked():
            self.engine.declare(Fact(smoothsort_be_used=True))
        if self.patienceSortingCb.isChecked():
            self.engine.declare(Fact(patience_sorting_be_used=True))
        if self.treeSortCb.isChecked():
            self.engine.declare(Fact(tree_sort_be_used=True))
        if self.bucketSortCb.isChecked():
            self.engine.declare(Fact(bucket_sort_be_used=True))
        if self.radixSortCb.isChecked():
            self.engine.declare(Fact(radix_sort_be_used=True))
        if self.countingSortCb.isChecked():
            self.engine.declare(Fact(counting_sort_be_used=True))
        if self.hanSortCb.isChecked():
            self.engine.declare(Fact(han_sort_be_used=True))
        if self.multithreadedMergeSortCb.isChecked():
            self.engine.declare(Fact(multithreaded_merge_sort_be_used=True))
        if self.psrsSorting.isChecked():
            self.engine.declare(Fact(PSRS_sorting_be_used=True))

        self.engine.declare(Fact(action='BACKWARD'))
        self.engine.run()

        self.engine.declare(Fact(final=True))
        self.engine.run()
        print(self.engine.facts)

        for i in self.engine.facts:

            try:
                self.comboBox.setCurrentIndex(get_key(best_time_selected, self.engine.facts[i]["best_time"]))
            except KeyError as e:
                no_error = 0
            try:
                self.comboBox_2.setCurrentIndex(get_key(average_time_selected, self.engine.facts[i]["average_time"]))
            except KeyError as e:
                no_error = 0
            try:
                self.comboBox_3.setCurrentIndex(get_key(worst_time_selected, self.engine.facts[i]["worst_time"]))
            except KeyError as e:
                no_error = 0
            try:
                self.comboBox_4.setCurrentIndex(get_key(memory_selected, self.engine.facts[i]["memory"]))
            except KeyError as e:
                no_error = 0
            try:
                if (self.engine.facts[i]["sustainability"] == 'Yes'):
                    self.radioButton_2.setChecked(True)
                elif (self.engine.facts[i]["sustainability"] == 'No'):
                    self.radioButton_3.setChecked(True)
                elif (self.engine.facts[i]["sustainability"] == 'skip'):
                    self.radioButton.setChecked(True)
            except KeyError as e:
                no_error = 0
            try:
                self.comboBox_5.setCurrentIndex(get_key(exchanges_selected, self.engine.facts[i]["exchanges"]))
            except KeyError as e:
                no_error = 0


    def reason(self):

        self.kill_check_box()
        print("----------------------------------------------------------")
        self.engine.reset()  # Prepare the engine for the execution.

        try:
            self.engine.declare(Fact(best_time=best_time_selected[self.comboBox.currentIndex()]))
        except KeyError as e:
            raise ValueError('Undefined unit: {}'.format(e.args[0]))

        try:
            self.engine.declare(Fact(average_time=average_time_selected[self.comboBox_2.currentIndex()]))
        except KeyError as e:
            raise ValueError('Undefined unit: {}'.format(e.args[0]))

        try:
            self.engine.declare(Fact(worst_time=worst_time_selected[self.comboBox_3.currentIndex()]))
        except KeyError as e:
            raise ValueError('Undefined unit: {}'.format(e.args[0]))

        try:
            self.engine.declare(Fact(memory=memory_selected[self.comboBox_4.currentIndex()]))
        except KeyError as e:
            raise ValueError('Undefined unit: {}'.format(e.args[0]))

        if (self.radioButton_2.isChecked()):
            self.engine.declare(Fact(sustainability="yes"))
        elif (self.radioButton_3.isChecked()):
            self.engine.declare(Fact(sustainability="no"))
        elif (self.radioButton.isChecked()):
            self.engine.declare(Fact(sustainability="skip"))

        try:
            self.engine.declare(Fact(exchanges=exchanges_selected[self.comboBox_5.currentIndex()]))
        except KeyError as e:
            raise ValueError('Undefined unit: {}'.format(e.args[0]))

        self.engine.declare(Fact(action='FORWARD'))
        self.engine.run()
        print(self.engine.facts)

        for f in self.engine.facts:
            try:
                self.bubbleSortCb.setChecked(self.engine.facts[f]['bubble_sort_may_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.insertionSortCb.setChecked(self.engine.facts[f]['insertion_sort_may_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.shellsortCb.setChecked(self.engine.facts[f]['shellsort_may_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.selectionSortCb.setChecked(self.engine.facts[f]['selection_sort_may_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.quickSortCb.setChecked(self.engine.facts[f]['quick_sort_may_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.mergeSortCb.setChecked(self.engine.facts[f]['merge_sort_may_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.timsortCb.setChecked(self.engine.facts[f]['timsort_may_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.heapSortCb.setChecked(self.engine.facts[f]['heap_sort_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.smoothsortCb.setChecked(self.engine.facts[f]['smoothsort_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.patienceSortingCb.setChecked(self.engine.facts[f]['patience_sorting_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.treeSortCb.setChecked(self.engine.facts[f]['tree_sort_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.bucketSortCb.setChecked(self.engine.facts[f]['bucket_sort_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.radixSortCb.setChecked(self.engine.facts[f]['radix_sort_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.countingSortCb.setChecked(self.engine.facts[f]['counting_sort_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.hanSortCb.setChecked(self.engine.facts[f]['han_sort_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.multithreadedMergeSortCb.setChecked(self.engine.facts[f]['multithreaded_merge_sort_be_used'])
            except KeyError as e:
                no_error = 0

            try:
                self.psrsSorting.setChecked(self.engine.facts[f]['PSRS_sorting_be_used'])
            except KeyError as e:
                no_error = 0



    def kill_check_box(self):
        self.bubbleSortCb.setChecked(False)
        self.insertionSortCb.setChecked(False)
        self.shellsortCb.setChecked(False)
        self.selectionSortCb.setChecked(False)
        self.quickSortCb.setChecked(False)
        self.mergeSortCb.setChecked(False)
        self.timsortCb.setChecked(False)
        self.heapSortCb.setChecked(False)
        self.smoothsortCb.setChecked(False)
        self.patienceSortingCb.setChecked(False)
        self.treeSortCb.setChecked(False)
        self.bucketSortCb.setChecked(False)
        self.radixSortCb.setChecked(False)
        self.countingSortCb.setChecked(False)
        self.hanSortCb.setChecked(False)
        self.multithreadedMergeSortCb.setChecked(False)
        self.psrsSorting.setChecked(False)


def get_key(dic, value):
    for k, v in dic.items():
        if v == value:
            return k


best_time_selected = {
    0: 'line',
    1: 'square',
    2: 'nlogn',
    3: 'nlog2n',
    4: 'nloglogn',
    5: 'nlgn',
    6: 'linePlusK',
    7: 'mutyTredLogn',
    8: 'skip'
}

average_time_selected = {
    0: 'depends_on_choice_of_step',
    1: 'square',
    2: 'nlogn',
    3: 'nlogkn',
    4: 'nlgn',
    5: 'linePlusK',
    6: 'nloglogn',
    7: 'mutyTredLogn',
    8: 'skip'
}

worst_time_selected = {
    0: 'square',
    1: 'square_unlikely',
    2: 'nlogn',
    3: 'lineMultK',
    4: 'nlgn',
    5: 'Ok',
    6: 'nloglogn',
    7: 'mutyTredLogn',
    8: 'skip'
}

memory_selected = {
    0: 'O(1)',
    1: 'O(logn)',
    2: 'OnO1',
    3: 'On',
    4: 'Ok',
    5: 'On2',
    6: 'skip'
}

exchanges_selected = {
    0: 'line',
    1: 'square',
    2: 'nlogn',
    3: 'unresolved',
    4: 'linePlusK',
    5: 'nloglogn',
    6: 'skip'

}

app = QtWidgets.QApplication(sys.argv)  # Новый экземпляр QApplication
window = App()  # Создаём объект класса ExampleApp
window.show()  # Показываем окно
app.exec_()  # и запускаем приложение

